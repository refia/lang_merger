export let enMap = {
    choose1lang: 'Choose your first lang file',
    choose2lang: 'Choose your second lang file',
    lang: 'Language',
    reservedAnNext: 'Mark as resolved and show next',
    parsingProgress: 'Parsing progress',
    noConflictLines: 'Ready lines',
    conflictLines: 'Conflicts found',
    conflictsResolved: 'Conflicts Resolved',
    autoConfirm: 'Don\'t require confirmation',
    generateAndDownload: 'Generate and Save Lang File',
    noAnyConflicts: 'There are no any conflicts. Now you may download merged .lang file',
    allConflictsWereResolved: 'All conflicts were resolved.',
};