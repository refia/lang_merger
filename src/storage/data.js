import { writable } from 'svelte/store';
export const langParsedData = writable(null);
export const config = writable(null);
export const translator = writable(null);