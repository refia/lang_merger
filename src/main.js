import App from './App.svelte';
import {ruMap} from './translateMaps/ruMap';
import {enMap} from './translateMaps/enMap';

const app = new App({
	target: document.body,
	props: {
		appName: 'Lang Merger',
		appVersion: 0.1,
		helper: {
			ruMap,
			enMap,
			downloadAsFile: (data, type, fileName) => {
				let file = new Blob([data], {type: (type || 'data:text/plain;charset=utf-8')});
				let a = document.createElement("a");
				let url = URL.createObjectURL(file);
				a.href = url;
				a.download = fileName;
				a.text = '';
				document.body.appendChild(a);
				a.style.display = 'none';
				document.body.appendChild(a);
				a.click();
				document.body.removeChild(a);
			},
			readData: (ev) => {
				return new Promise((resolve, reject) => {
					let file = ev.target.files[0];
					let fr = new FileReader();
					fr.onload = function (res) {
						resolve(res.target.result);
					};
					fr.readAsText(file);
				});
			}
		}
	}
});

export default app;


